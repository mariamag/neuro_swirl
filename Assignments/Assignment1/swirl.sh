#!/bin/bash

#echo is printing a string. 
#echo -ne '/'; #Here the trailing new line is not outputed and a forward slash is printed to the screen
#sleep 1;
#echo -ne '\b-'; #Here the e allows for backslash to be interpreted and then allows for a back space to happen and prints a dash 
#sleep 1;
#echo -ne '\b\\' #Here there is a backspace and then a double backslash allows for a double backslash to be printed
#sleep 1;
#echo -ne '\b|'; #Here backspace and then a straight line
#sleep 1;
#echo -ne '\b/'; #Here backspace and a forward slash
#sleep 1;
#echo -ne '\b-'; #Here backspace and a dash
#sleep 1;
#echo -ne '\b\\';#Again backspace and a backslash
#sleep 1;
#echo -ne '\b|'; #Again backspace and a straight line
#echo -ne "\b \n"; #backspace and then a new line


#Opposite Direction swirl.sh with added speed
#echo -ne '\'; 
#sleep 0.1;
#echo -ne '\b-';
#sleep 0.1;
#echo -ne '\b/';
#sleep 0.1;
#echo -ne '\b|';
#sleep 0.1;
#echo -ne '\b\\';
#sleep 0.1;
#echo -ne '\b-';
#sleep 0.1;
#echo -ne '\b/';
#sleep 0.1;
#echo -ne '\b|';
#echo -ne '\b\n';

#Opposite Direction swirl.sh with added speed

echo "A for loop... within a for loop"
sleep 1.5
echo "Wait for it...."
sleep 1.0
echo "And"
sleep 0.8
echo "Go!"
for i in `seq 1 10`; 
do
    echo -ne '\'; 
    sleep 0.1;
    echo -ne '\b-';
    sleep 0.1;
    echo -ne '\b/';
    sleep 0.1;
    echo -ne '\b|';
    sleep 0.1;
    echo -ne '\b\\';
    sleep 0.1;
    echo -ne '\b-';
    sleep 0.1;
    echo -ne '\b/';
    sleep 0.1;
    echo -ne '\b|';
    echo -ne '\b\n';

    for i in `seq 1 10`; 
    do 
	echo -ne '/'; 
	sleep 0.01;
	echo -ne '\b-';  
	sleep 0.01;
	echo -ne '\b\\'
	sleep 0.01;
	echo -ne '\b|'; 
	sleep 0.01;
	echo -ne '\b/'; 
	sleep 0.01;
	echo -ne '\b-'; 
	sleep 0.01;
	echo -ne '\b\\';
	sleep 0.01;
	echo -ne '\b|'; 
	echo -ne "\b \n"; 
    done
done
echo "So impressive!"
