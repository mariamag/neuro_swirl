#!/bin/bash

#Create a series of files in a file directory 
#Called Donor_Files

#this will make a directory to store the files
mkdir donor;

# this for loop creates the 50 donor files
for i in `seq 1 50`
do
    for j in `seq 1 10` ## nested looop to create donor files with the ten time points
    do
	echo "Creating file: file${i}_tp${j}.txt"; 
	touch donor/donor${i}_tp${j}.txt;
	echo "data" >> donor/donor${i}_tp${j}.txt ## this creates header
	echo $RANDOM >> donor/donor${i}_tp${j}.txt ## this and the copies below create the random data points
	echo $RANDOM >> donor/donor${i}_tp${j}.txt
	echo $RANDOM >> donor/donor${i}_tp${j}.txt
	echo $RANDOM >> donor/donor${i}_tp${j}.txt
	echo $RANDOM >> donor/donor${i}_tp${j}.txt; 
    done

done



