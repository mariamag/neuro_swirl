options(stringsAsFactors = FALSE)
library(tidyverse)

## pass argument to this script
args <- commandArgs(trailingOnly = TRUE)
message <- paste0("This is job", args[i])
print(message)